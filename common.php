<?php

require 'vendor/autoload.php';

// Pour avoir la date en français (dans le tweet)
setlocale(LC_ALL, 'fr_FR.UTF-8');
$date = strtotime('yesterday');

// Tableau avec en clé le nom auprès de TMVL, en valeur le label pour le tweet
$counters = [
    'Pont Wilson'              => 'Pont Wilson',
    'Pont Napoléon TOTAL'      => 'Pont Napoléon',
    'Pont Saint Sauveur TOTAL' => 'Pont Saint-Sauveur',
    'Pont Sanitas TOTAL'       => 'Pont de Sanitas',
    'Afficheur Pont de Fil'    => 'Pont de fil',
    'Pont Tramway'             => 'Pont du tram',
    'Pont d\'Arcole #2'        => 'Pont d\'Arcole',
    'Quai de la Loire'         => 'Quai de la Loire',
    'Point O'                  => 'Point O',
    'Rue de Buffon'            => 'Rue de Buffon',
    'Boulevard Heurteloup'     => 'Boulevard Heurteloup',
    'Rue Edouard Vaillant'     => 'Rue Édouard Vaillant',
    'Rue d\'Entraigues'        => 'Rue d\'Entraigues',
];

// On charge DotEnv pour lire le fichier de configuration .env
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

$settings = [
    'account_id'          => $_ENV['TWITTER_ACCOUNT_ID'],
    'access_token'        => $_ENV['TWITTER_OAUTH_ACCESS_TOKEN'],
    'access_token_secret' => $_ENV['TWITTER_OAUTH_ACCESS_TOKEN_SECRET'],
    'consumer_key'        => $_ENV['TWITTER_CONSUMER_KEY'],
    'consumer_secret'     => $_ENV['TWITTER_CONSUMER_SECRET'],
    'bearer_token'        => $_ENV['TWITTER_BEARER_TOKEN']
];
