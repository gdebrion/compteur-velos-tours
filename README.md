# Compteur Vélos Tours

Ce projet a pour but de récupérer le décompte quotidien des passages de cyclistes à certains endroits stratégiques de la ville et de les publier sur Twitter sur le compte [@CyclistesATours](https://twitter.com/CyclistesATours).

## Installation 

```
composer install
```

## Configuration
Renommer le fichier _.env.example_ en _.env_ et éditez-le afin de mettre votre configuration personnelle.

## Utilisation

Pour avoir les statistiques de la veille :

```
php yesterday.php
```