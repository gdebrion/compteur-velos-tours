<?php

require 'common.php';

// Lecture de l'API
$opendata = new RestClient([
    'base_url' => 'https://data.tours-metropole.fr/api/records/1.0/',
    'format'   => 'json',
]);

$data = [];
// Pour tous les compteurs…
foreach ($counters as $key => $label) {
    $results = $opendata->get('search', [
        'q'                   => 'date:[' . date('Y-m-d', strtotime('first day of last month')) . ' TO ' . date('Y-m-d', strtotime('last day of last month')) . ']',
        'dataset'             => 'comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine',
        'refine.nom_compteur' => $key,
        'facet'               => [
            'nom_compteur',
            'counts',
        ],
        'rows'                => 1000,
    ]);

    // Est-ce qu'on a quelque chose  ?
    if ($results->info->http_code == 200) {
        $response = $results->decode_response();

        foreach ($response->records as $record) {
            $date = substr($record->fields->date, 0, 10);

            if (!isset($data[$label])) {
                $data[$label] = [];
            }

            if (!isset($data[$label][$date])) {
                $data[$label][$date] = $record->fields->counts;
            } else {
                // On applique la même logique que pour les comptages quotidiens
                $data[$label][$date] = max($data[$label][$date], $record->fields->counts);
            }
        }

        // On somme tous les résultats quotidiens en un résultat mensuel
        if (isset($data[$label])) {
            $data[$label] = array_sum($data[$label]);
        }
    }
}

// Tri décroissant du jeu de résultats
arsort($data);

// On retire les résultats nuls
$data = array_filter($data, function ($item) {
    return $item > 0;
});

// Génération de la phrase
if (count($data) > 0) {
    $counterstr = [];
    $str        = "En " . strftime("%B %Y", strtotime('last month')) . " à Tours :\n";

    foreach ($data as $counter => $total) {
        $counterstr[] = "→ " . $counter . ' : ' . number_format($total, 0, ',', ' ') . ' 🚲';
    }

    $tweet = $str . implode("\n", $counterstr);
    while (strlen($tweet) > 280) {
        array_pop($counterstr);
        $tweet = $str . implode("\n", $counterstr);
    }

    // Arrivé ici, nous avons un Tweet de moins de 280 caractères
    $twitter = new Noweh\TwitterApi\Client($settings);
    $twitter->tweet()->create()->performRequest([
        'text' => ucfirst($tweet)
    ]);

    echo $tweet;
}
