<?php

require 'common.php';

// Lecture de l'API
$opendata = new RestClient([
    'base_url' => 'https://data.tours-metropole.fr/api/records/1.0/',
    'format'   => 'json',
]);

$data = [];
// Pour tous les compteurs…
foreach ($counters as $key => $label) {
    $results = $opendata->get('search', [
        'dataset'             => 'comptage-velo-donnees-compteurs-syndicat-des-mobilites-de-touraine',
        'refine.date'         => date('Y-m-d', $date),
        'refine.nom_compteur' => $key,
        'facet'               => [
            'nom_compteur',
            'counts',
        ],
    ]);

    // Est-ce qu'on a quelque chose  ?
    if ($results->info->http_code == 200) {
        $response = $results->decode_response();
        $value    = 0;
        /**
         * Attention, on peut avoir plusieurs lignes pour un seul compteur.
         * J'ai eu l'explication : on a le compteur et parfois l'afficheur.
         *
         * L'afficheur communique en temps réel alors que le compteur le matin à 6h.
         *
         * Il faut donc prendre la valeur la plus grande des deux (le compteur), la
         * valeur la plus faible étant les cyclistes de 0 à 6h du matin.
         */
        foreach ($response->records as $record) {
            $value = max($value, $record->fields->counts);
        }

        $data[$label] = $value;
    }
}

// Tri décroissant du jeu de résultats
arsort($data);

// On retire les résultats nuls
$data = array_filter($data, function ($item) {
    return $item > 0;
});

// Génération de la phrase
if (count($data) > 0) {
    $counterstr = [];
    $str        = strftime("%A %e %B %Y", $date) . " à Tours :\n";

    foreach ($data as $counter => $total) {
        $counterstr[] = "→ " . $counter . ' : ' . number_format($total, 0, ',', ' ') . ' 🚲';
    }

    $tweet = $str . implode("\n", $counterstr);
    while (strlen($tweet) > 280) {
        array_pop($counterstr);
        $tweet = $str . implode("\n", $counterstr);
    }

    // Arrivé ici, nous avons un Tweet de moins de 280 caractères
    $twitter = new Noweh\TwitterApi\Client($settings);
    $twitter->tweet()->create()->performRequest([
        'text' => ucfirst($tweet)
    ]);

    // echo $tweet;
}
